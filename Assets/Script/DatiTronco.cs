using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName =("TruncData"),menuName =("SceneObjectData"),order = 0)]
public class DatiTronco : ScriptableObject
{
    public Sprite TruncBase;
    public Sprite TruncMid;
    public Sprite TruncTop;
}
