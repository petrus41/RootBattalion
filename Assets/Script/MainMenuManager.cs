using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private List<Spawner> spawners = new List<Spawner>();
    [SerializeField]
    private GameObject[] GameLengthIdicator = new GameObject[3];
    [SerializeField]
    private float[] GameLength = new float[3];
    [SerializeField]
    private GameTimeDisplay GameTimeDisplay;
    [HideInInspector]
    public int TimeIndex = 0;
    [SerializeField]
    MenuController[] Controller = new MenuController[2];

    [SerializeField]
    private GameObject Camera;


    private void Start()
    {
        GameLengthIdicator[TimeIndex].transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
    }
    public void SwitchTimeIndex(int i)
    {
        GameLengthIdicator[TimeIndex].transform.localScale = new Vector3(1f, 1f, 1f);
        if (i < 0)
        {
            TimeIndex--;
        }
        else
        {
            TimeIndex++;
        }
        GameLengthIdicator[TimeIndex].transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
    }
    private void StartGame()
    {
        GameTimeDisplay.GameMaxTime = GameLength[TimeIndex];
        GameTimeDisplay.bIsGameStarted = true;
        foreach (Spawner Sp in spawners)
        {
            Sp.SetUp();
        }
        foreach (MenuController MC in Controller)
        {
            MC.gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }
    private void PanCamera()
    {
        Camera.transform.DOMoveY(0,2).OnComplete(StartGame);
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Space))
        {
            PanCamera();
        }
    }
}
