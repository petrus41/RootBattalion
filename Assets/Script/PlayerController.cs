using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Tronco PlayerTrunc;
    [SerializeField]
    private int PlayerNr;
    [SerializeField]
    private float AutoMoveIntervall;
    private float AutoMoveTime;
    public bool bCanAutoMove;
    public Tronco GetTrunc()
    {
        return PlayerTrunc;
    }

    void AutoMove()
    {
        Root[] AllDirection = new Root[4];
        foreach (Energy En in PlayerTrunc.Energies)
        {
            AllDirection = En.ActualPosition.GetAllNeighbours();
            int CenterDirection = 1 + (PlayerNr * 2);
            int OutDirection = 3 - (PlayerNr * 2);
            if (AllDirection[(int)Direction.S] && En.ActualPosition.GetParent() != AllDirection[(int)Direction.S])
            {
                En.Move(Direction.S);
            }
            else if (AllDirection[CenterDirection] && En.ActualPosition.GetParent() != AllDirection[CenterDirection])
            {
                En.Move((Direction)CenterDirection);
            }
            else if (AllDirection[OutDirection] && En.ActualPosition.GetParent() != AllDirection[OutDirection])
            {
                En.Move((Direction)OutDirection);
            }
            else if(AllDirection[(int)Direction.N] && En.ActualPosition.GetParent() != AllDirection[(int)Direction.N])
            {
                En.Move(Direction.N);
            }
            else
            {
                En.Move(Direction.S);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        CheckInput();
        if (bCanAutoMove)
        {
            AutoMoveTime += Time.deltaTime;
            if (AutoMoveTime >= AutoMoveIntervall)
            {
                AutoMove();
                AutoMoveTime -= AutoMoveIntervall;
            }
        }
    }
    void CheckInput()
    {
        if (Input.GetKeyDown(Controlls.South[PlayerNr]))
        {
            MoveAllEnergy(Direction.S);
            AutoMoveTime = 0;
        }
        else if (Input.GetKeyDown(Controlls.West[PlayerNr]))
        {
            MoveAllEnergy(Direction.W);
            AutoMoveTime = 0;
        }
        else if (Input.GetKeyDown(Controlls.East[PlayerNr]))
        {
            MoveAllEnergy(Direction.E);
            AutoMoveTime = 0;
        }
        else if ((Input.GetKeyDown(Controlls.North[PlayerNr])))
        {
            MoveAllEnergy(Direction.N);
            AutoMoveTime = 0;
        }
    }
    private void MoveAllEnergy(Direction Dir)
    {
        List<Energy> ToDeleAte = new List<Energy>();
        foreach (Energy En in PlayerTrunc.Energies)
        {
            if (En.GetComponent<PoisonEnergy>())
                continue;
            if (!En.Move(Dir))
            {
                ToDeleAte.Add(En);
            }
        }

        foreach (Energy En in ToDeleAte)
        {
            PlayerTrunc.Energies.Remove(En);
            Destroy(En.gameObject);
        }
    }
}
