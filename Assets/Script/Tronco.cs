using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tronco : MonoBehaviour
{
    public int PlayerNr;
    [SerializeField]
    DatiTronco[] Data;
    public int DataIndex;
    private Root StartingRoot;
    public Vector3 Offset;
    [SerializeField]
    GameObject RootPrefab;
    [SerializeField]
    Vector3 OffsetUp;

    private GameObject TreeTip;
    [SerializeField]
    GameObject TreeTile;

    public Material LongestTile;
    public Material NormalTile;

    [SerializeField]
    RootSprites mSprites;

    public List<Energy> Energies = new List<Energy>();

    private int MaxPoints = 0;
    private Root maxPointRoot;

    public Spawner EnemyTrunc;

    public int GetMaxPoints()
    {
        return MaxPoints;
    }
    public void PreviewFullTree()
    {
        TreeTip.GetComponent<SpriteRenderer>().sprite = Data[DataIndex].TruncTop;
        UpdateSprite();
    }

    
    public void UpdateSprite()
    {
        GetComponent<SpriteRenderer>().sprite = Data[DataIndex].TruncBase;
    }
    public Root GetStartingRoot()
    {
        return StartingRoot;
    }
    public void OnWaterReached(int score, Root r)
    {
        if (score < MaxPoints) { return; }

        if (maxPointRoot)
        {
            SetMatRecursiveUp(maxPointRoot, NormalTile);
        }
        MaxPoints = score;
        maxPointRoot = r;
        SetMatRecursiveUp(r, LongestTile);
        Debug.Log(string.Format("Player {0} Touched water, now points: {1}", PlayerNr, score));
    }
    private void SetMatRecursiveUp(Root root, Material mat)
    {
        Root traverse = root;
        while (traverse != null)
        {
            traverse.GetComponentInChildren<SpriteRenderer>().material = mat;
            traverse = traverse.GetParent();
        }
    }

    private void Awake()
    {
        mSprites.Init();

        GameObject newObject = Instantiate(RootPrefab);
        newObject.transform.position = transform.position + Offset;
        StartingRoot = newObject.GetComponent<Root>();
        StartingRoot.SetTrunk(this);
        StartingRoot.PlayerNr = PlayerNr;
        StartingRoot.SetSprites(mSprites);
        StartingRoot.Generate(Direction.S);
        GameObject temp = Instantiate(TreeTile);
        temp.transform.position = transform.position + OffsetUp;
        TreeTip = temp;
    }



}
