using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    GameObject EnergyToSpawn;
    public virtual void OnUse(Tronco User)
    {
        User.gameObject.GetComponent<Spawner>().ChangePreview(EnergyToSpawn);
    }
}
