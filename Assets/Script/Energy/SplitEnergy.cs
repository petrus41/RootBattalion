using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitEnergy : Energy
{
    override public bool Move(Direction dir)
    {
        Root neighbour = ActualPosition.GetNeighbour(dir);
        if (neighbour == null)
        {
            // Move to empty? Create new roots
            bool createdNewRoot = ActualPosition.Generate(dir);
            if (createdNewRoot)
            {
                // Fork from newly created root!
                Root newNeighbour = ActualPosition.GetNeighbour(dir);

                for (int i = 0; i < 4; i++)
                {
                    newNeighbour.Generate((Direction)i);
                }
            }
            return !createdNewRoot;
        }
        else if (neighbour == ActualPosition.GetParent())
        {
            // Trying to move back to where we came from, nope
            return true;
        }
        else
        {
            // Move to one of the children? Carry on
            ActualPosition = ActualPosition.GetNeighbour(dir);
            transform.position = ActualPosition.transform.position + OffSet;
            return true;
        }
    }
}
