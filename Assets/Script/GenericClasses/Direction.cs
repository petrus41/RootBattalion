using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    N = 0,
    E,
    S,
    W,
}
