using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class RootSprites
{
    // 4 connections; connects to ..
    public Sprite Cross;

    // 3 connections; connects to ..
    public Sprite NorthEastSouth;
    public Sprite EastSouthWest;
    public Sprite NorthSouthWest;
    public Sprite NorthWestEast;

    // 2 connections; connects to ..
    public Sprite NorthWest;
    public Sprite NorthEast;
    public Sprite SouthWest;
    public Sprite SouthEast;
    public Sprite EastWest;
    public Sprite NorthSouth;

    // 1 connection; connects to ..
    public Sprite North;
    public Sprite East;
    public Sprite South;
    public Sprite West;

    private Sprite[] mSprites;

    public void Init()
    {
        mSprites = new Sprite[16]
        {
            North, // hax
            North,
            East,
            NorthEast,
            South,
            NorthSouth,
            SouthEast,
            NorthEastSouth,
            West,
            NorthWest,
            EastWest,
            NorthWestEast,
            SouthWest,
            NorthSouthWest,
            EastSouthWest,
            Cross,
        };
    }

    public Sprite Select(Root root)
    {
        var score = 0
            | (root.GetNeighbour(Direction.N) ? 1 : 0) << 0
            | (root.GetNeighbour(Direction.E) ? 1 : 0) << 1
            | (root.GetNeighbour(Direction.S) ? 1 : 0) << 2
            | (root.GetNeighbour(Direction.W) ? 1 : 0) << 3;

        return mSprites[score];
    }
}

