using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Controlls
{
    public static KeyCode[] North = new KeyCode[] { KeyCode.W, KeyCode.UpArrow };
    public static KeyCode[] East = new KeyCode[] { KeyCode.D, KeyCode.RightArrow };
    public static KeyCode[] South = new KeyCode[] { KeyCode.S, KeyCode.DownArrow };
    public static KeyCode[] West = new KeyCode[] { KeyCode.A, KeyCode.LeftArrow };
}
