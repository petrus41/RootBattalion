using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : MonoBehaviour
{
    private Root[] Connetion = new Root[4];
    private Root mParent;
    private Tronco mTrunk;
    private RootSprites mSprites;


    public GameObject RootPrefab;
    public int PlayerNr;

    public static float Step = 1.0f;
    public static Vector2[] GrowStep =  {
        new Vector2(0, Step),
        new Vector2(Step, 0),
        new Vector2(0, -Step),
        new Vector2(-Step, 0),
    };
    public Root GetNeighbour(Direction Dir)
    {
        return Connetion[(int)Dir];
    }
    public Root[] GetAllNeighbours()
    {
        return Connetion;
    }
    public Root GetParent() { return mParent; }
    public void SetSprites(RootSprites sprites)
    {
        mSprites = sprites;
        UpdateSprite();
    }

    public void UpdateSprite()
    {
        SpriteRenderer renderer = GetComponentInChildren<SpriteRenderer>();
        renderer.sprite = mSprites.Select(this);
    }
    public void SetTrunk(Tronco t)
    {
        mTrunk = t;
    }
    Direction InvertDirection(Direction dir)
    {
        if (dir == Direction.N) { return Direction.S; }
        else if (dir == Direction.E) { return Direction.W; }
        else if (dir == Direction.S) { return Direction.N; }
        /* W */
        return Direction.E;
    }
    public bool Generate(Direction dir, bool bCanCut = false)
    {
        Vector3 stepDirection = GrowStep[(int)dir];

        gameObject.GetComponent<Collider2D>().enabled = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, stepDirection, stepDirection.magnitude);
        Debug.DrawRay(transform.position, stepDirection, Color.yellow, 5.0f);
        gameObject.GetComponent<Collider2D>().enabled = true;

        if (hit.collider != null)
        {
            bool allowGrowing = hit.collider.gameObject.CompareTag("Tree");
            // Touched water, traverse and highlight
            if (hit.collider.gameObject.CompareTag("FinishLine"))
            {
                int pointsForThisBranch = 0;
                Root traverse = this;
                while (traverse != null)
                {

                    pointsForThisBranch++;
                    traverse = traverse.GetParent();
                }

                mTrunk.OnWaterReached(pointsForThisBranch, this);
                return true;
            }
            else if (hit.collider.gameObject.CompareTag("Root") && bCanCut)
            {
                Root Hitted = hit.collider.gameObject.GetComponent<Root>();
                if (Hitted.mTrunk != mTrunk)
                {
                    allowGrowing = true;
                    List<Root> ToCheck = new List<Root>();
                    foreach (Root roo in Hitted.GetAllNeighbours())
                    {
                        ToCheck.Add(roo);
                    }
                    Destroy(Hitted.gameObject);
                    List<Root> ToAdd = new List<Root>();
                    while (ToCheck.Count > 0)
                    {
                        foreach (Root roo in ToCheck)
                        {
                            if (!roo.GetParent())
                            {
                                foreach (Root ro in roo.GetAllNeighbours())
                                {
                                    ToAdd.Add(ro);
                                }
                            }
                        }
                        ToCheck.Clear();
                        ToCheck = ToAdd;
                    }
                }
            }
            else if (hit.collider.gameObject.CompareTag("PowerUp"))
            {
                allowGrowing = true;
                PowerUp Hitted = hit.collider.gameObject.GetComponent<PowerUp>();
                Hitted.OnUse(mTrunk);
                Destroy(Hitted.gameObject);
            }
            if (!allowGrowing)
            {
                return false;
            }
        }

        var spawnPosition = transform.position + stepDirection;
        // Do not allow spawning above starting root height (so NO SPAWNS ON SURFACE)
        if (spawnPosition.y >= mTrunk.GetStartingRoot().transform.position.y)
        {
            return false;
        }

        GameObject newRootObj = Instantiate(RootPrefab);
        newRootObj.transform.position = spawnPosition;
        newRootObj.name = transform.position.ToString();
        Root newRoot = newRootObj.GetComponent<Root>();
        newRoot.PlayerNr = PlayerNr;
        newRoot.Connetion[(int)InvertDirection(dir)] = this;
        newRoot.mParent = this;
        newRoot.SetSprites(mSprites);
        newRoot.SetTrunk(mTrunk);

        Connetion[(int)dir] = newRoot;
        UpdateSprite();
        return true;
    }
}
