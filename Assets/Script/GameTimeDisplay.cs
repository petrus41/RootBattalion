using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class GameTimeDisplay : MonoBehaviour
{
    [SerializeField]
    Image Display;
    [HideInInspector]
    public float GameMaxTime;
    private float GameTime;
    [HideInInspector]
    public bool bIsGameStarted;


    [SerializeField]
    GameObject ControllerObj;
    PlayerController[] Controllers = new PlayerController[2];

    public float GetGameTime()
    {
        return GameTime;
    }
    public bool isGameStarted()
    {
        return bIsGameStarted;
    }
    private void Awake()
    {
        Controllers = GetComponents<PlayerController>();
    }
    void SearchWinner()
    {
        if (Controllers[0].GetTrunc().GetMaxPoints() > Controllers[1].GetTrunc().GetMaxPoints())
        {

        }
        else
        {

        }
    }
    private void Update()
    {
        if (bIsGameStarted)
        {
            GameTime += Time.deltaTime;
            if (GameTime >= GameMaxTime)
            {
                SearchWinner();
                bIsGameStarted = false;

            }
            Display.fillAmount = 1 - (GameTime / GameMaxTime);
        }
    }
}
