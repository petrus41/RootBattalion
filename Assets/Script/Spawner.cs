using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject EnergyPrefab;


    [HideInInspector] public GameObject nextEnergy;

    private Tronco mTrunc;
    public bool bCanSpawn;
    [SerializeField]
    float SpawnTimerIntervall;
    private float SpawnTime;


    public void SetUp()
    {
        mTrunc = GetComponent<Tronco>();
        GameObject temp = Instantiate(EnergyPrefab);
        temp.transform.position = transform.position + new Vector3(0, 0, -1);
        nextEnergy = temp;
        bCanSpawn = true;

    }
    public void ChangePreview(GameObject ToEnergy)
    {
        nextEnergy.SetActive(false);
        GameObject temp = Instantiate(ToEnergy);
        temp.transform.position = transform.position + new Vector3(0, 0, -1);
        nextEnergy = temp;
    }
    private void Update()
    {
        if (bCanSpawn)
        {
            SpawnTime += Time.deltaTime;
            if (SpawnTime >= SpawnTimerIntervall)
            {
                SpawnTime -= SpawnTimerIntervall;
                Spawn();
            }
        }
    }

    void Spawn()
    {
        if (nextEnergy)
        {
            Energy temp = nextEnergy.GetComponent<Energy>();
            mTrunc.Energies.Add(temp);
            temp.ActualPosition = mTrunc.GetStartingRoot();
            nextEnergy.transform.position = temp.ActualPosition.transform.localPosition + new Vector3(0, 0, -1);
            GameObject newTemp = Instantiate(EnergyPrefab);
            newTemp.transform.position = transform.position + new Vector3(0, 0, -1);
            nextEnergy = newTemp;
        }
    }

}
