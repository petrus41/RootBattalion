using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    bool bIsSetUp;
    [SerializeField]
    Vector2 FieldSize;
    [SerializeField]
    GameObject[] PowerUpPrefab;
    private GameTimeDisplay gameTimeDisplay;
    private float powerUpTime;
    private float powerUpTimer;
    public static float Step = 1.0f;
    public static Vector2[] GrowStep =  {
        new Vector2(0, Step),
        new Vector2(Step, 0),
        new Vector2(0, -Step),
        new Vector2(-Step, 0),
    };
    private bool CanSpawn;
    private void Awake()
    {
        gameTimeDisplay = GetComponent<GameTimeDisplay>();
    }
    private void SpawnPowerUp()
    {
        Vector3 SpawnPosition = new Vector3(0, 0, 0);
        while (!CanSpawn)
        {
            CanSpawn = true;
            SpawnPosition.y = (Random.value * FieldSize.y) - (FieldSize.y / 2);
            SpawnPosition.x = (Random.value * FieldSize.x) - (FieldSize.x / 2);
            for (int i = 0; i < 4; i++)
            {
                Vector3 stepDirection = GrowStep[i];              
                RaycastHit2D hit = Physics2D.Raycast(SpawnPosition, stepDirection, stepDirection.magnitude);
                Debug.DrawRay(SpawnPosition, stepDirection, Color.red, 5.0f);
                if (hit.collider)
                    CanSpawn = false;
            }

        }
        int randomIndex = Random.Range(0, PowerUpPrefab.Length);
        GameObject Temp = Instantiate(PowerUpPrefab[randomIndex]);
        Temp.transform.position = new Vector3(SpawnPosition.x, SpawnPosition.y, -1);
        powerUpTime = 10 - (gameTimeDisplay.GetGameTime() * 4 / gameTimeDisplay.GameMaxTime);
        CanSpawn = false;
    }

    private void Update()
    {
        if (!gameTimeDisplay.isGameStarted())
            return;
        if(!bIsSetUp)
        {
            powerUpTime = 10 - (gameTimeDisplay.GetGameTime() * 4 / gameTimeDisplay.GameMaxTime);
            bIsSetUp = true;
        }
        powerUpTime = 10 - (gameTimeDisplay.GetGameTime() * 4 / gameTimeDisplay.GameMaxTime);
        powerUpTimer += Time.deltaTime;
        if (powerUpTimer >= powerUpTime)
        {
            powerUpTimer -= powerUpTime;
            SpawnPowerUp();
        }
    }

}
