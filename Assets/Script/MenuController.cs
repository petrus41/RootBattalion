using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    private int PlayerNr;
    [SerializeField]
    Tronco PlayerTrunc;
    [SerializeField]
    MainMenuManager Manager;
    private void Start()
    {
        PlayerTrunc.PreviewFullTree();
    }
    private void Update()
    {
        CheckInput();
    }
    private bool TrySwitchTimeIndex(int i)
    {
        if (i > 0 && Manager.TimeIndex + 1 < 3)
        {
            return true;
        }
        else if (i < 0 && Manager.TimeIndex - 1 >= 0)
        {
            return true;
        }
        return false;
    }
    private bool TrySwitchTruncIndex(int i)
    {
        if (i > 0 && PlayerTrunc.DataIndex + 1 < 4)
        {
            return true;
        }
        else if (i < 0 && PlayerTrunc.DataIndex - 1 >= 0)
        {
            return true;
        }
        return false;
    }

    void CheckInput()
    {
        if (Input.GetKeyDown(Controlls.South[PlayerNr]))
        {
            if(TrySwitchTimeIndex(-1))
            {
                Manager.SwitchTimeIndex(-1);
            }
        }
        else if (Input.GetKeyDown(Controlls.West[PlayerNr]))
        {
            if (TrySwitchTruncIndex(1))
            {
                PlayerTrunc.DataIndex++;
                PlayerTrunc.PreviewFullTree();
            }
        }
        else if (Input.GetKeyDown(Controlls.East[PlayerNr]))
        {
            if (TrySwitchTruncIndex(-1))
            {
                PlayerTrunc.DataIndex--;
                PlayerTrunc.PreviewFullTree();
            }
        }
        else if ((Input.GetKeyDown(Controlls.North[PlayerNr])))
        {
            if (TrySwitchTimeIndex(1))
            {
                Manager.SwitchTimeIndex(1);
            }
        }
    }
}
