using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonEnergy : Energy
{
    override public bool Move(Direction dir)
    {
        var neighbour = ActualPosition.GetNeighbour(dir);
        if (neighbour == null)
        {
            // Move to empty? Create new roots
            var createdNewRoot = ActualPosition.Generate(dir, true);
            return !createdNewRoot;
        }
        else if (neighbour == ActualPosition.GetParent())
        {
            // Trying to move back to where we came from, nope
            return true;
        }
        else
        {
            // Move to one of the children? Carry on
            ActualPosition = ActualPosition.GetNeighbour(dir);
            transform.position = ActualPosition.transform.position + OffSet;
            return true;
        }
    }
}
